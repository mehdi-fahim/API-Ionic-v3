import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { NewsApiService } from '../../services/newsapi.service';
import { NewsApiGlobal } from '../../model/newsapi-global.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  news: NewsApiGlobal = new NewsApiGlobal();

  constructor(public navCtrl: NavController, private newsApiService: NewsApiService,) {
  
    this.newsApiService.getArticles()
    .then(newsFetched => {
      this.news = newsFetched;
      console.log(this.news);
    })

  }

}
