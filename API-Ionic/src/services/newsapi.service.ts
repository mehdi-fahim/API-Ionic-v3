// Core components
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

// RxJS
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

// Model
import { NewsApiGlobal } from '../model/newsapi-global.model';

@Injectable()
export class NewsApiService {

    private API_URL: string = 'https://newsapi.org/v2/';
    private source: string = 'ign';
    private API_KEY: string = '0d1752b02b82439ca7bec9d3c79117fc';

    constructor( private http: Http){}

    public getArticles(): any {

        const url = `${this.API_URL}top-headlines?sources=${this.source}&apiKey=${this.API_KEY}`; 
        
        return this.http.get(url)
        .toPromise()
        .then(response => response.json() as NewsApiGlobal)
        .catch(error => console.log("Une erreur est survenue" + error))
    }
}