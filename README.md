## Api modulable de Ionic 3

Source pour les API ==>  [newsapi.org](newsapi.org)

___

***Model***

```javascript
export class NewsApiGlobal {

    status: string;
    articles: {
        source: {
                id: string;
                name: string;
            };
        author: string;
        title: string;
        description: string;
        url: string;
        urlToImage: string;
        publishedAt: string;
    }
}
```
___

## Run project

```
npm install
```

```
ionic serve
```
